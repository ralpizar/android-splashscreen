package info.rafanet.splashscreendemo;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    public class Tarea extends TimerTask {

        @Override
        public void run() {
            Log.w("Texto2", "Tarea3");

            Intent i = new Intent("splash1");
            sendBroadcast(i);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void buttonAccion(View v) {
        Intent intent = new Intent(this, Splash.class);
        startActivity(intent);

        Tarea tarea = new Tarea();

        Timer tiempo = new Timer();
        tiempo.schedule(tarea, 2000);

        Toast.makeText(this, "Test", Toast.LENGTH_SHORT).show();
    }

}
